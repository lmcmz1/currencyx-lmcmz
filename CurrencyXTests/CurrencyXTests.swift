//
//  CurrencyXTests.swift
//  CurrencyXTests
//
//  Created by lmcmz on 21/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import XCTest
@testable import CurrencyX

class CurrencyXTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testCurrency() {
        let AUD = Currency.init(symbol: "AUD")
        XCTAssertNotNil(AUD)
        
        let XAU = Currency.XAU
        XCTAssertNotEqual(AUD, XAU)
        
        let AUDModel = Currency.init(model: CurrencyModel.australiaModel)
        XCTAssertEqual(AUD, AUDModel)
    }
    
    func testParseJSON() {
        let jsonString = """
        {
          "buyNotes" : "1.6967",
          "SpotRate_Date_Fmt" : "20200124",
          "LASTUPDATED" : "09:01 AM 24 Jan 2020",
          "buyTC" : "On App",
          "currencyName" : "Pa anga",
          "sellNotes" : "1.4422",
          "effectiveDate_Fmt" : "20200124T091207,88+11:00",
          "buyTT" : "1.6911",
          "updateDate_Fmt" : "20200124T091207,88+11:00",
          "country" : "Tonga",
          "sellTT" : "1.4830",
          "currencyCode" : "TOP"
        }
        """
        let model = CurrencyModel.deserialize(from: jsonString)
        XCTAssertNotNil(model)
        
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
