Pod::Spec.new do |spec|
  spec.name         = "Currency_X_Lmcmz"
  spec.version      = "0.0.3"
  spec.summary      = "Currency SDK for currency exchange base on AUD."
  spec.description  = "Currency SDK for currency exchange base on AUD."
  spec.homepage     = "https://gitlab.com/lmcmz1/programming-assessment"
  spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  spec.author       = { "lmcmz" => "lmcmze@gmail.com" }

  spec.source       = { :git => "https://gitlab.com/lmcmz1/programming-assessment.git", :tag => "#{spec.version}" }

  spec.ios.deployment_target = '10.0'
  spec.source_files  = "CurrencyX/Source/**/*"
  spec.swift_version = '5.0'
  
  spec.dependency 'HandyJSON', '~> 5.0.1'
  spec.dependency 'Moya', '~> 13.0'
  spec.dependency 'PromiseKit', '~> 6.8'
  
end
