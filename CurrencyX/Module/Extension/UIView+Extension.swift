//
//  UIView+Extension.swift
//  CurrencyX
//
//  Created by lmcmz on 22/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func flash() {
        let flash = CABasicAnimation(keyPath: "opacity")
          flash.duration = 0.4
          flash.fromValue = 1
          flash.toValue = 0.1
          flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
          flash.autoreverses = true
        flash.repeatCount = .infinity
          layer.add(flash, forKey: nil)
    }
}
