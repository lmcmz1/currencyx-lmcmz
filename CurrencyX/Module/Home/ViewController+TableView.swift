//
//  ViewController+TableView.swift
//  CurrencyX
//
//  Created by lmcmz on 22/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyCell.nameOfClass, for: indexPath) as! CurrencyCell
        let toCurrency = data[indexPath.row]
        
        var selectedCurrency: CurrencyModel!
        if activeCell == -1 {
            selectedCurrency = CurrencyModel.australiaModel
        } else {
            selectedCurrency = data[activeCell]
        }
        
        cell.configuration(fromCurrency: selectedCurrency,
                           amount: currentAmount,
                           toCurrency: toCurrency,
                           displayText: currentCalculateText)
        
//        cell.isSelected = activeCell == indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        activeCell = indexPath.row
        panelVC.reset()
        headerView.isSelected = false
//        let cell = tableView.cellForRow(at: indexPath)
//        cell?.isSelected = true
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var selectedCurrency: CurrencyModel!
        if activeCell == -1 {
            selectedCurrency = CurrencyModel.australiaModel
        } else {
            selectedCurrency = data[activeCell]
        }
        
        headerView.configuration(fromCurrency: selectedCurrency,
                                 amount: currentAmount,
                                 toCurrency: CurrencyModel.australiaModel,
                                 displayText: currentCalculateText)
        return headerView
    }
    
    // MARK: - Edit
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if let image = cell.reorderControlImageView {
//            image.tint(color: .white)
//        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.data[sourceIndexPath.row]
        data.remove(at: sourceIndexPath.row)
        data.insert(movedObject, at: destinationIndexPath.row)
    }
}
