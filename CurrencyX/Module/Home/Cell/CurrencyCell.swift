//
//  CurrencyCell.swift
//  CurrencyX
//
//  Created by lmcmz on 22/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell {

    @IBOutlet var currencyImage: UIImageView!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var currencyNameLabel: UILabel!
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var rateLabel: UILabel!
    @IBOutlet var activeView: UIView!
    @IBOutlet var calculateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        /// TODO: Add multiple currency support
        activeView.isHidden = !selected
        if selected {
            activeView.flash()
            contentView.backgroundColor = UIColor(hex: "3B4261")
        } else {
            activeView.layer.removeAllAnimations()
            contentView.backgroundColor = UIColor(hex: "262A3E")
        }
    }
    
    func configuration(fromCurrency: CurrencyModel,
                       amount: Double = 1.0,
                       toCurrency: CurrencyModel,
                       displayText: String? = nil) {
        
        currencyLabel.text = toCurrency.currencyCode
        currencyNameLabel.text = toCurrency.currencyName
        let countryName = toCurrency.country.lowercased()
        currencyImage.image = UIImage(named: countryName)
        countryLabel.text = countryName.uppercased()
        calculateLabel.text = fromCurrency == toCurrency ? displayText : ""
        
        if fromCurrency == toCurrency {
            rateLabel.text = amount.removeZerosFromEnd()
            return
        }
        
        onBackgroundThread {
            if let value = ExchangeRate.shared.convertToAmountSync(from: Currency(model: fromCurrency)!,
                                                                   fromAmount: amount,
                                                                   to: Currency(model: toCurrency)!) {
                onMainThread {
                    self.rateLabel.text = value.rounded(toPlaces: 3).removeZerosFromEnd()
                }
                
            } else {
                onMainThread {
                    self.rateLabel.text = "ERROR"
                }
            }
        }
        
    }
}
