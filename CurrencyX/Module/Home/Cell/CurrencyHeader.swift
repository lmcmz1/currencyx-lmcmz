//
//  CurrencyHeader.swift
//  CurrencyX
//
//  Created by lmcmz on 22/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import UIKit

class CurrencyHeader: UITableViewHeaderFooterView {
    
    @IBOutlet var currencyImage: UIImageView!
    @IBOutlet var currencyLabel: UILabel!
    @IBOutlet var currencyNameLabel: UILabel!
    @IBOutlet var countryLabel: UILabel!
    @IBOutlet var rateLabel: UILabel!
    @IBOutlet var activeView: UIView!
    @IBOutlet var calculateLabel: UILabel!
    
    var clickBlock: (() -> Void)?
    
    var isSelected: Bool = true {
        didSet {
            activeView.isHidden = !isSelected
            if isSelected {
                activeView.flash()
            } else {
                activeView.layer.removeAllAnimations()
            }
        }
    }
    
    class func instanceFromNib() -> CurrencyHeader {
        let view = UINib(nibName: nameOfClass, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CurrencyHeader
        return view
    }
    
    override func awakeFromNib() {
        isSelected = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapHeader))
        self.addGestureRecognizer(tap)
    }
    
    @objc func tapHeader() {
        isSelected = true
        guard let block = clickBlock else {
            return
        }
        block()
    }
    
    func configuration(fromCurrency: CurrencyModel,
                       amount: Double = 1.0,
                       toCurrency: CurrencyModel,
                       displayText: String? = nil) {
        
        currencyLabel.text = toCurrency.currencyCode
        currencyNameLabel.text = toCurrency.currencyName
        let countryName = toCurrency.country.lowercased()
        currencyImage.image = UIImage(named: countryName)
        countryLabel.text = countryName.uppercased()
        calculateLabel.text = fromCurrency == toCurrency ? displayText : ""
        
        if fromCurrency == toCurrency {
            self.rateLabel.text = amount.removeZerosFromEnd()
            return
        }
        
        onBackgroundThread {
            if let value = ExchangeRate.shared.convertToAmountSync(from: Currency(model: fromCurrency)!,
                                                                   fromAmount: amount,
                                                                   to: Currency(model: toCurrency)!) {
                onMainThread {
                    self.rateLabel.text = value.rounded(toPlaces: 3).removeZerosFromEnd()
                }
                
            } else {
                onMainThread {
                    self.rateLabel.text = "ERROR"
                }
            }
        }
    }
}
