//
//  ViewController+Panel.swift
//  CurrencyX
//
//  Created by lmcmz on 22/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import Foundation
import FloatingPanel

extension ViewController: FloatingPanelControllerDelegate, FloatingPanelBehavior {
    
    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return MyFloatingPanelLayout()
    }
    
    func allowsRubberBanding(for edge: UIRectEdge) -> Bool {
        return true
    }
}

extension ViewController: CalculatorPanelDelegate {
    
    func errorInput(error: CalculatorError) {
        errorAlert(text: error.errorDescription)
    }
    
    func panelButtonClicked(displayString: String, value: Double) {
        currentCalculateText = displayString
        currentAmount = value
    }
}

class MyFloatingPanelLayout: FloatingPanelLayout {
    public var initialPosition: FloatingPanelPosition {
        return .half
    }
    
    var supportedPositions: Set<FloatingPanelPosition> {
        return [.half, .tip]
    }

    public func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        switch position {
        case .half:
            return 300.0 // A bottom inset from the safe area
        case .tip:
            return 44.0 // A bottom inset from the safe area
        default:
            return nil // Or `case .hidden: return nil`
        }
    }
}
