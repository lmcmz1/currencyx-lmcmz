//
//  ViewController.swift
//  CurrencyX
//
//  Created by lmcmz on 21/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import UIKit
import ESPullToRefresh
import FloatingPanel

class ViewController: UIViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var emojiLabel: UILabel!
    
    var data: [CurrencyModel] = []
    var fpc: FloatingPanelController!
    var currentAmount: Double = 100.0 {
        didSet {
            tableView.reloadData()
            if activeCell != -1 {
                tableView.selectRow(at: IndexPath(row: activeCell, section: 0), animated: false, scrollPosition: .none)
            }
        }
    }
    var currentCalculateText: String?
    var activeCell: Int = -1
    
    lazy var headerView: CurrencyHeader = {
        
        let header = CurrencyHeader.instanceFromNib()
        header.clickBlock = {
            let cell = self.tableView.cellForRow(at: IndexPath(row: self.activeCell, section: 0))
            cell?.isSelected = false
            self.activeCell = -1
            self.panelVC.reset()
        }
        return header
    }()
    
    lazy var panelVC: CalculatorPanelVC = {
        let vc = CalculatorPanelVC()
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        tableView.es.startPullToRefresh()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        fpc.removePanelFromParent(animated: false)
    }
    
    func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 300, right: 0)
        tableView.registerHeaderFooter(nibName: CurrencyHeader.nameOfClass)
        tableView.registerCell(nibName: CurrencyCell.nameOfClass)
        tableView.es.addPullToRefresh {
            self.requestData()
        }
        
        fpc = FloatingPanelController()
        fpc.surfaceView.backgroundColor = UIColor(hex: "1C1F2E")
        fpc.delegate = self
        panelVC.delegate = self
        fpc.set(contentViewController: panelVC)
        fpc.addPanel(toParent: self)
        
        var nsDictionary: NSDictionary?
        if let path = Bundle.main.path(forResource: "Configuration", ofType: "plist"),
            let key = Bundle.main.infoDictionary?["Configuration"] {
            nsDictionary = NSDictionary(contentsOfFile: path)
            
            if let dict = nsDictionary?[key] as? NSDictionary, let emoji = dict["face"] as? String {
                emojiLabel.text = emoji
            }
        }
    }
    
    func requestData() {
        ExchangeRate.getAllRateBaseAUD().done { list in
            list.filter { $0.buyNotes != nil && $0.buyTT != nil }.forEach { currency in
                if self.data.contains(currency), let index = self.data.firstIndex(of: currency) {
                    self.data[index] = currency
                } else {
                    self.data.append(currency)
                }
            }
//            self.tableView.reloadData()
            
        }.ensure {
            self.tableView.reloadData()
            if self.activeCell != -1 {
                self.tableView.selectRow(at: IndexPath(row: self.activeCell, section: 0), animated: false, scrollPosition: .none)
            }
            self.tableView.es.stopPullToRefresh()
        }.catch { error in
            print(error.localizedDescription)
            self.errorAlert(text: "Failed to fetch data")
        }
    }
    
    // MARK: - Action
    
    @IBAction func reorderButtonClick() {
        tableView.isEditing = !tableView.isEditing
    }
    
    // MARK: - Error

    func errorAlert(text: String) {
        errorAnimation()
        titleLabel.text = text
        titleLabel.textColor = UIColor.systemRed
        delay(3) {
            self.errorAnimation()
            self.titleLabel.text = "Currency X 💰"
            self.titleLabel.textColor = UIColor(hex: "FFD479")
        }
    }

    func errorAnimation() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        transition.type = CATransitionType(rawValue: "cube")
        transition.subtype = CATransitionSubtype.fromBottom
        titleLabel.layer.add(transition, forKey: "country1_animation")
    }
}
