//
//  CalculatorPanelVC.swift
//  CurrencyX
//
//  Created by lmcmz on 22/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import UIKit

protocol CalculatorPanelDelegate: class {
    func panelButtonClicked(displayString: String, value: Double)
    func errorInput(error: CalculatorError)
}

class CalculatorPanelVC: UIViewController {
    
    var recentOperation: CalculatorOperation?
    var resultText: String = ""
    var resultValue: Double = 100.00
    
    @IBOutlet var deleteButton: BaseUIControl!
    
    weak var delegate: CalculatorPanelDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressDelete))
        deleteButton.addGestureRecognizer(longPress)
    }
    
    func reset() {
        recentOperation = nil
        resultValue = 100
        resultText = ""
        delegate?.panelButtonClicked(displayString: resultText, value: resultValue)
    }
    
    @objc func longPressDelete(_ gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            recentOperation = nil
            resultValue = 100
            resultText = ""
            delegate?.panelButtonClicked(displayString: resultText, value: resultValue)
        default:
            break
        }
    }
    
    @IBAction func calculatorButtonClick( button: BaseUIControl ) {
        let tag = button.tag
        
        guard let operation = CalculatorOperation(rawValue: tag) else {
            delegate?.errorInput(error: .unKnowOperation)
            return
        }
        
        switch operation {
        case .delete:
            resultText = String(resultText.dropLast())
        case .add, .subtract, .multiply, .divide:
            
            guard let text = resultText.last, let lastInput = CalculatorOperation(char: text) else {
                return
            }
            
            if lastInput.isOperation {
                delegate?.errorInput(error: .moreThanOneOperation)
                return
            }
            
//            containOperator = false
            if resultText.contains(CalculatorOperation.add.displayCharacter!) ||
                resultText.contains(CalculatorOperation.divide.displayCharacter!) ||
                resultText.contains(CalculatorOperation.multiply.displayCharacter!) ||
                resultText.contains(CalculatorOperation.subtract.displayCharacter!) {
//                containOperator = true
                
                recentOperation = operation
                resultText = "\(resultValue.removeZerosFromEnd())\(String(operation.displayCharacter!))"
            } else {
                recentOperation = operation
                resultText += String(operation.displayCharacter!)
            }
            
        default: // 0-9 .dot
            resultText += String(operation.displayCharacter!)
        }
        
        // Empty
        if resultText.count == 0 {
            recentOperation = nil
            resultValue = 100
            delegate?.panelButtonClicked(displayString: resultText, value: resultValue)
            return
        }
        
        guard let recent = recentOperation, let char = recent.displayCharacter else {
//            noCalculationValue(text: resultText)
//            resultValue = 100
            
            if let doubleValue = Double(resultText) {
                delegate?.panelButtonClicked(displayString: resultText, value: doubleValue)
                return
            }
            
            delegate?.errorInput(error: .unKnowOperation)
            return
        }
        
        let array = resultText.split(separator: char)
        
        switch array.count {
        case 0:
            delegate?.panelButtonClicked(displayString: resultText, value: resultValue)
        case 1:
            noCalculationValue(text: String(array.first!))
        case 2:
            calculteValue()
        default:
            delegate?.errorInput(error: .moreThanOneOperation)
        }
    }
    
    func noCalculationValue(text: String) {
        guard let value = Double(text) else {
            delegate?.errorInput(error: .invaildDouble)
            return
        }
        resultValue = value
        delegate?.panelButtonClicked(displayString: resultText, value: resultValue)
    }
    
    func calculteValue() {
        let stringArray = resultText.split(separator: (recentOperation?.displayCharacter)!)
        
        guard stringArray.count == 2 else {
            delegate?.errorInput(error: .moreThanOneOperation)
            return
        }
        
        guard let num1 = Double(stringArray.first!), let num2 = Double(stringArray.last!) else {
            delegate?.errorInput(error: .invaildDouble)
            return
        }
        
        switch recentOperation {
        case .add:
            resultValue = num1 + num2
        case .divide:
            resultValue = num1 / num2
        case .multiply:
            resultValue = num1 * num2
        case .subtract:
            resultValue = num1 - num2
        default:
            break
        }
        
        delegate?.panelButtonClicked(displayString: resultText, value: resultValue)
    }
}
    
