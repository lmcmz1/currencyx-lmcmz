//
//  Calculator.swift
//  CurrencyX
//
//  Created by lmcmz on 26/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import Foundation

enum CalculatorError: Error {
    case unKnowOperation
    case invaildDouble
    case moreThanOneOperation
    
    public var errorDescription: String {
        switch self {
        case .invaildDouble:
            return "Invaild Double"
        case .moreThanOneOperation:
            return "Can't process"
        case .unKnowOperation:
            return "Unknow Operation"
        }
    }
}


enum CalculatorOperation: Int {
    case zero = 0
    case one, two, three, four, five,
    six, seven, eight, nine
    
    case dot = 10
    case delete = 11
    
    case add = 12
    case subtract = 13
    case multiply = 14
    case divide = 15
    
    var displayCharacter: Character? {
        switch self {
        case .zero:
            return "0"
        case .one:
            return "1"
        case .two:
            return "2"
        case .three:
            return "3"
        case .four:
            return "4"
        case .five:
            return "5"
        case .six:
            return "6"
        case .seven:
            return "7"
        case .eight:
            return "8"
        case .nine:
            return "9"
        case .dot:
            return "."
        case .delete:
            return nil
        case .add:
            return "+"
        case .subtract:
            return "-"
        case .multiply:
            return "×"
        case .divide:
            return "÷"
        }
    }
    
    var isOperation: Bool {
        switch self {
        case .add, .subtract, .multiply, .divide:
            return true
        default:
            return false
        }
    }
    
    init?(char: Character) {
        switch char {
        case CalculatorOperation.add.displayCharacter:
            self = .add
        case CalculatorOperation.subtract.displayCharacter:
            self = .subtract
        case CalculatorOperation.multiply.displayCharacter:
            self = .multiply
        case CalculatorOperation.divide.displayCharacter:
            self = .divide
        case CalculatorOperation.zero.displayCharacter:
            self = .zero
        case CalculatorOperation.one.displayCharacter:
            self = .one
        case CalculatorOperation.two.displayCharacter:
            self = .two
        case CalculatorOperation.three.displayCharacter:
            self = .three
        case CalculatorOperation.four.displayCharacter:
            self = .four
        case CalculatorOperation.five.displayCharacter:
            self = .five
        case CalculatorOperation.six.displayCharacter:
            self = .six
        case CalculatorOperation.seven.displayCharacter:
            self = .seven
        case CalculatorOperation.eight.displayCharacter:
            self = .eight
        case CalculatorOperation.nine.displayCharacter:
            self = .nine
        default:
            return nil
        }
    }
}
