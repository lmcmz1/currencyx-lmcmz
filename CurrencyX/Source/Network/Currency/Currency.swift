//
//  Currency.swift
//  CurrencyX
//
//  Created by lmcmz on 21/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import Foundation

public enum Currency: CaseIterable, Equatable {
    
    case
    AUD, USD, EUR, GBP, NZD,
    CNY, AED, ARS, BDT, BND,
    BRL, CAD, CHF, CLP, DKK,
    FJD, HKD, IDR, INR, JPY,
    KRW, LKR, MYR, NOK, PGK,
    PHP, PKR, SAR, SBD, SEK,
    SGD, THB, TOP, TWD, VND,
    VUV, WST, XPF, ZAR, XAU,
    CNH
    
    case Unknow(String)
    
    public static var allCases: [Currency] {
        let defaultList: [Currency] = [ AUD, USD, EUR, GBP, NZD,
                                        CNY, AED, ARS, BDT, BND,
                                        BRL, CAD, CHF, CLP, DKK,
                                        FJD, HKD, IDR, INR, JPY,
                                        KRW, LKR, MYR, NOK, PGK,
                                        PHP, PKR, SAR, SBD, SEK,
                                        SGD, THB, TOP, TWD, VND,
                                        VUV, WST, XPF, ZAR, XAU,
                                        CNH]
        return defaultList
    }
    
    var symbol: String {
        switch self {
        
        case .AUD:
            return "AUD"
        case .USD:
            return "USD"
        case .EUR:
            return "EUR"
        case .GBP:
            return "GBP"
        case .NZD:
            return "NZD"
        case .CNY:
            return "CNY"
        case .AED:
            return "AED"
        case .ARS:
            return "ARS"
        case .BDT:
            return "BDT"
        case .BND:
            return "BND"
        case .BRL:
            return "BRL"
        case .CAD:
            return "CAD"
        case .CHF:
            return "CHF"
        case .CLP:
            return "CLP"
        case .DKK:
            return "DKK"
        case .FJD:
            return "FJD"
        case .HKD:
            return "HKD"
        case .IDR:
            return "IDR"
        case .INR:
            return "INR"
        case .JPY:
            return "JPY"
        case .KRW:
            return "KRW"
        case .LKR:
            return "LKR"
        case .MYR:
            return "MYR"
        case .NOK:
            return "NOK"
        case .PGK:
            return "PGK"
        case .PHP:
            return "PHP"
        case .PKR:
            return "PKR"
        case .SAR:
            return "SAR"
        case .SBD:
            return "SBD"
        case .SEK:
            return "SEK"
        case .SGD:
            return "SGD"
        case .THB:
            return "THB"
        case .TOP:
            return "TOP"
        case .TWD:
            return "TWD"
        case .VND:
            return "VND"
        case .VUV:
            return "VUV"
        case .WST:
            return "WST"
        case .XPF:
            return "XPF"
        case .ZAR:
            return "ZAR"
        case .XAU:
            return "XAU"
        case .CNH:
            return "CNH"
        case .Unknow(let symbol):
            return symbol
        }
    }
    
    init?(symbol: String) {
        let currency =  Currency.allCases.filter{ $0.symbol == symbol }
        if currency.count != 0 {
            self = currency.first!
        } else {
            self = Currency.Unknow(symbol)
        }
    }
    
    init?(model: CurrencyModel) {
        if let currency = Currency(symbol: model.currencyCode) {
            self = currency
        } else {
            return nil
        }
    }
    
    public static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.symbol == rhs.symbol
    }

}
