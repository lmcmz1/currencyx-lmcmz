//
//  CurrencyModel.swift
//  CurrencyX
//
//  Created by lmcmz on 21/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import Foundation
import HandyJSON

public struct CurrenciesDictModel: HandyJSON {
    var Products: Dictionary<String, Any>!
    
    public init() {
    }
}

public struct CurrencyModel: HandyJSON, Equatable {
    
    var currencyCode: String!
    var currencyName: String!
    var country: String!
    var buyTT: Double!
    var sellTT: Double!
    var buyTC: Double!
    var buyNotes: String?
    var sellNotes: String?
    var SpotRate_Date_Fmt: Date?
    var effectiveDate_Fmt: String?
    var updateDate_Fmt: String?
    var LASTUPDATED: String?
    
    public static var australiaModel: CurrencyModel {
        var model = CurrencyModel()
        model.currencyCode = "AUD"
        model.currencyName = "Dollars"
        model.country = "Australia"
        
        /// TODO: Not sure about the sell and buy in AUD
        model.buyNotes = "1"
        model.buyTT = 1.0
        model.sellTT = 1.0
        model.sellNotes = "1"
        return model
    }
    
    public init() {
    }
    
    public static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.currencyCode == rhs.currencyCode
    }
    
//    mutating func update(newModel: CurrencyModel) {
//
//        if newModel.buyTT != "N/A" {
//            buyTT = newModel.buyTT
//        }
//
//        if newModel.sellTT != "N/A" {
//            sellTT = newModel.sellTT
//        }
//
//    }
    
    public mutating func mapping(mapper: HelpingMapper) {
        mapper <<<
        self.buyTT <-- TransformOf<Double, String>(fromJSON: { (rawString) -> Double? in
            
            if rawString == "N/A" {
                return nil
            }
            
            guard let value = Double(rawString!) else {
                return nil
            }
            return value
            
        }, toJSON: { value -> String in
            
            return String(value!)
        })
        
        mapper <<<
        self.sellTT <-- TransformOf<Double, String>(fromJSON: { (rawString) -> Double? in
            
            if rawString == "N/A" {
                return nil
            }
            
            guard let value = Double(rawString!) else {
                return nil
            }
            return value
            
        }, toJSON: { value -> String in
            
            return String(value!)
        })
        
        mapper <<<
        self.buyNotes <-- TransformOf<String?, String?>(fromJSON: { rawString -> String? in
            if rawString == "N/A" {
                return nil
            }
            return rawString!
        }, toJSON: { string -> String? in
            return string!
        })
        
        mapper <<<
        self.sellNotes <-- TransformOf<String?, String?>(fromJSON: { rawString -> String? in
            if rawString == "N/A" {
                return nil
            }
            return rawString!
        }, toJSON: { string -> String? in
            return string!
        })
        
        mapper <<<
        self.buyTC <-- TransformOf<Double, String>(fromJSON: { (rawString) -> Double? in
            
            if rawString == "N/A" {
                return nil
            }
            
            guard let value = Double(rawString!) else {
                return nil
            }
            return value
            
        }, toJSON: { value -> String in
            
            return String(value!)
        })
        
        mapper <<<
        self.SpotRate_Date_Fmt <-- CustomDateFormatTransform(formatString: "yyyyMMdd")
    }
}
