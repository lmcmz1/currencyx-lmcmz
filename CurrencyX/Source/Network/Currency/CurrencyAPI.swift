//
//  WestPacCurrencyAPI.swift
//  CurrencyX
//
//  Created by lmcmz on 21/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import Moya

enum CurrencyAPI {
    case fxData
}

extension CurrencyAPI: TargetType {
    var headers: [String: String]? {
        return nil
    }

    var baseURL: URL {
        return URL(string: "https://www.westpac.com.au")!
    }

    var path: String {
        switch self {
        case .fxData:
            return "/bin/getJsonRates.wbc.fx.json"
        }
    }

    var method: Moya.Method {
        return .get
    }

    var task: Task {
        return .requestPlain
    }

    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
}
