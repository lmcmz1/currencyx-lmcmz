//
//  API+Util.swift
//  CurrencyX
//
//  Created by lmcmz on 21/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import Foundation
import HandyJSON
import Moya

extension Response {
    func mapObject<T: HandyJSON>(_: T.Type) -> T? {
        guard let dataString = String(data: self.data, encoding: .utf8),
            let object = JSONDeserializer<T>.deserializeFrom(json: dataString)
        else {
            return nil
        }

        return object
    }

    func mapObject<T: HandyJSON>(_: T.Type, designatedPath: String) -> T? {
        guard let dataString = String(data: self.data, encoding: .utf8),
            let object = JSONDeserializer<T>.deserializeFrom(json: dataString, designatedPath: designatedPath)
        else {
            return nil
        }

        return object
    }

    func mapArray<T: HandyJSON>(_: T.Type) -> [T?]? {
        guard let dataString = String(data: self.data, encoding: .utf8),
            let object = JSONDeserializer<T>.deserializeModelArrayFrom(json: dataString)
        else {
            return nil
        }
        return object
    }

    func mapArray<T: HandyJSON>(_: T.Type, designatedPath: String) -> [T?]? {
        guard let dataString = String(data: self.data, encoding: .utf8),
            let object = JSONDeserializer<T>.deserializeModelArrayFrom(
                json: dataString, designatedPath: designatedPath
            )
        else {
            return nil
        }
        return object
    }
}
