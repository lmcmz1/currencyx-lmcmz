//
//  API.swift
//  CurrencyX
//
//  Created by lmcmz on 21/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import Foundation
import HandyJSON
import Moya
import PromiseKit

enum MyError: Error {
    case FoundNil(String)
    case DecodeFailed
}

func API<T: HandyJSON, U: TargetType>(_ target: U, path: String? = nil) -> Promise<T> {
    
    return Promise<T> { seal in
        let provider = MoyaProvider<U>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])
        provider.request(target, completion: { result in
            switch result {
            case let .success(response):

                if let designPath = path, !designPath.isEmpty {
                    guard let model = response.mapObject(T.self, designatedPath: designPath) else {
                        seal.reject(MyError.DecodeFailed)
                        return
                    }
                    seal.fulfill(model)

                } else {
                    guard let model = response.mapObject(T.self) else {
                        seal.reject(MyError.DecodeFailed)
                        return
                    }
                    seal.fulfill(model)
                }

            case let .failure(error):
                seal.reject(error)
            }
        })
    }
}

/// This function returns a *hello* string for a given `subject`.
///
/// - Warning: The returned string is not localized.
///
/// Usage:
///
///     println(hello("Markdown")) // Hello, Markdown!
///
/// - Parameter subject: The subject to be welcomed.
///
/// - Returns: A hello string to the `subject`.

func API<T: HandyJSON, U: TargetType>(_ target: U, path: String? = nil) -> Promise<[T?]> {
    
    return Promise<[T?]> { seal in
        
        let provider = MoyaProvider<U>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])
        
        provider.request(target, completion: { result in
            switch result {
            case let .success(response):

                if let designPath = path, !designPath.isEmpty {
                    guard let model = response.mapArray(T.self, designatedPath: designPath) else {
                        seal.reject(MyError.DecodeFailed)
                        return
                    }
                    seal.fulfill(model)

                } else {
                    guard let model = response.mapArray(T.self) else {
                        seal.reject(MyError.DecodeFailed)
                        return
                    }
                    seal.fulfill(model)
                }

            case let .failure(error):
                seal.reject(error)
            }
        })
    }
}

func API<U: TargetType>(_ target: U, path: String? = nil) -> Promise<String> {
    
    return Promise<String> { seal in
        
        let provider = MoyaProvider<U>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])
        
        provider.request(target, completion: { result in
            switch result {
            case let .success(response):
                
                guard let dataString = String(data: response.data, encoding: .utf8) else{
                    seal.reject(MyError.DecodeFailed)
                    return
                }
                
                seal.fulfill(dataString)
                
            case let .failure(error):
                seal.reject(error)
            }
        })
    }
}

func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData = try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data // fallback to original data if it can't be serialized.
    }
}


