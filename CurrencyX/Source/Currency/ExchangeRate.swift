//
//  ExchangeRate.swift
//  CurrencyX
//
//  Created by lmcmz on 21/1/20.
//  Copyright © 2020 lmcmz. All rights reserved.
//

import Foundation
import PromiseKit
import Moya

public class ExchangeRate {
    
    public static let shared = ExchangeRate()
    var exchangeInfo: [CurrencyModel]?
    
    let expireTime: TimeInterval = 60 * 10  // Expire in 10 mins
    var lastUpdateTime: Date?
    
    // MARK: - Fetch data
    
    /// Get real time exchange rate of a *list* of currencies base on AUD (Austrlian Dollars)
    ///
    /// - Returns: A list of `CurrencyModel` which contains exchange rate information
    
    public class func getAllRateBaseAUD( block: @escaping (([CurrencyModel]?) -> Void) ) {
        ExchangeRate.getAllRateBaseAUD().done { list in
            var newList = list
            newList.append(CurrencyModel.australiaModel)
            ExchangeRate.shared.exchangeInfo = newList
            ExchangeRate.shared.lastUpdateTime = Date()
            block(newList)
        }.catch {_ in
            block(nil)
        }
    }
    
    /// This function returns a *Promise<list>* of currency exchange rate base on AUD (Austrlian Dollars)
    ///
    /// Usage:
    ///
    ///     ExchangeRate.getAllRateBaseAUD().done { list in
    ///         ...
    ///     }.catch { error in
    ///         ...
    ///     }
    ///
    /// - Returns: A Promise Object with a list of `CurrencyModel` which contains exchange rate information
    
    public class func getAllRateBaseAUD() -> Promise<[CurrencyModel]> {
        return Promise<[CurrencyModel]> { seal in
            firstly { () -> Promise<String> in
                API(CurrencyAPI.fxData)
            }.done { jsonString in
                guard let currenciesDict = CurrenciesDictModel.deserialize(from: jsonString,
                                                                           designatedPath: "data.Brands.WBC.Portfolios.FX") else {
                    seal.reject(MyError.DecodeFailed)
                    return
                }
                
                let list = currenciesDict.Products.keys.compactMap { currency in
                    return CurrencyModel.deserialize(from: currenciesDict.Products, designatedPath: "\(currency).Rates.\(currency)")
                }
                
                var newList = list
                newList.append(CurrencyModel.australiaModel)
                ExchangeRate.shared.exchangeInfo = newList
                ExchangeRate.shared.lastUpdateTime = Date()
                seal.fulfill(newList)
            }.catch { error in
                seal.reject(error)
            }
        }
    }
    
    // MARK: - Convertor
    
    /// This function calculate from amount of currencies to other currency in *Synchronization*.
    ///
    /// Usage:
    ///
    ///     DispatchQueue.global(qos: .background).async {
    ///         let amount = ExchangeRate.shared.convertToAmountSync(from: .AUD,  fromAmount: 100, to: .USD)
    ///     }
    ///
    /// - Parameter from: The original Currency. Default value is AUD
    /// - Parameter to: The destination Currency.
    /// - Parameter amount: The amount of original currency to convert. Default value is 1.0
    ///
    /// - Returns: The amount of destination currency have been converted.
    public func convertToAmountSync(from: Currency = .AUD, fromAmount: Double = 1.0, to: Currency) -> Double? {
        do {
            return try convertToAmount(from: from, fromAmount: fromAmount, to: to).wait()
        } catch {
            return nil
        }
    }
    
    /// This function calculate from amount of currencies to other currency in *Asynchronization*.
    ///
    /// Usage:
    ///
    ///     ExchangeRate.shared.convertToAmount(from: .AUD,  fromAmount: 100, to: .USD)
    ///
    /// - Parameter from: The original Currency. Default value is AUD
    /// - Parameter to: The destination Currency.
    /// - Parameter amount: The amount of original currency to convert. Default value is 1.0
    ///
    /// - Returns: The amount of destination currency have been converted.
    public func convertToAmount(from: Currency = .AUD, fromAmount: Double = 1.0, to: Currency) -> Promise<Double?> {
        
        return Promise<Double?> { seal in
            
            getCurrentInfo().done { data in
                let fromCurrencies = data.filter { $0.currencyCode == from.symbol }
                let toCurrencies = data.filter { $0.currencyCode == to.symbol }
                
                guard let fromCurrency = fromCurrencies.first,
                    let sellTT = fromCurrency.sellTT,
                    let toCurrency = toCurrencies.first,
                    let buyTT = toCurrency.buyTT else {
                    seal.fulfill(nil)
                    return
                }
                
                let demandInAUD = fromAmount / sellTT
                let amountTo = demandInAUD * buyTT
                seal.fulfill(amountTo)
            }.catch { error in
                seal.reject(error)
            }
        }
    }
    
    /// This function calculate the amount of from currencies to exchange the specific amount to in *Synchronization*.
    ///
    /// Usage:
    ///
    ///     DispatchQueue.global(qos: .background).async {
    ///         let amount = ExchangeRate.shared.convertFromAmountSync(from: .AUD, to: .USD, toAmount: 100)
    ///     }
    ///
    /// - Parameter from: The original Currency. Default value is AUD
    /// - Parameter to: The destination Currency.
    /// - Parameter amount: The amount of original currency to convert. Default value is 1.0
    ///
    /// - Returns: The amount of destination currency have been converted.
    public func convertFromAmountSync(from: Currency, to: Currency = .AUD, toAmount: Double = 1.0) -> Double? {
        do {
            return try convertFromAmount(from: from, to: to, toAmount: toAmount).wait()
        } catch {
            return nil
        }
    }
    
    /// This function calculate the amount of from currencies to exchange the specific amount to in *Asynchronization*.
    ///
    /// Usage:
    ///
    ///     ExchangeRate.shared.convertFromAmount(from: .AUD, to: .USD, toAmount: 100)
    ///
    /// - Parameter from: The original Currency. Default value is AUD
    /// - Parameter to: The destination Currency.
    /// - Parameter toAmount: The amount of destination currency to convert. Default value is 1.0
    ///
    /// - Returns: The amount of destination currency have been converted.
    
    public func convertFromAmount(from: Currency, to: Currency = .AUD, toAmount: Double = 1.0) -> Promise<Double?> {
        
        return Promise<Double?> { seal in
            
            getCurrentInfo().done { data in
                let fromCurrencies = data.filter { $0.currencyCode == from.symbol }
                let toCurrencies = data.filter { $0.currencyCode == to.symbol }
                
                guard let fromCurrency = fromCurrencies.first,
                    let toCurrency = toCurrencies.first,
                    let buyTT = toCurrency.buyTT,
                    let sellTT = fromCurrency.sellTT else {
                    seal.fulfill(nil)
                    return
                }
                
                let demandInAUD = toAmount / buyTT
                let amountFrom = demandInAUD * sellTT
                seal.fulfill(amountFrom)
                
            }.catch { error in
                seal.reject(error)
            }
        }
    }
    
    // MARK: - Update Singleton Data
    
    /// This function returns a *list* of exchange rate info.
    /// The data will expire in 10 mins, if the data is valid.
    /// It will return the list directly without requesting new data.
    /// If it's expired, it will make a request
    ///
    /// Usage:
    ///
    ///     ExchangeRate.shared.getCurrentInfo(forceRefresh: false)
    ///
    /// - Parameter forceRefresh: Set true to force update the exchange rate data
    ///
    /// - Returns: A *list* of exchange rate info.
    
    public func getCurrentInfo(forceRefresh: Bool = false) -> Promise<[CurrencyModel]> {
        
        if forceRefresh {
            return ExchangeRate.getAllRateBaseAUD()
        }
        
        guard let lastUpdate = lastUpdateTime else {
            return ExchangeRate.getAllRateBaseAUD()
        }
        
        return Promise<[CurrencyModel]> { seal in
            
            if lastUpdate.timeIntervalSinceNow < expireTime, let data = ExchangeRate.shared.exchangeInfo {
                seal.fulfill(data)
                return
            }
                    
            ExchangeRate.getAllRateBaseAUD().done { data in
                seal.fulfill(data)
            }.catch { error in
                seal.reject(error)
            }
        }
    }
    
}
