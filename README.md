

#![Banner](./image/Banner.png)

## Overview

CurrencyX is a simple Currency calculator which supports **41** different currencies. It all base on **AUD** (Australian Dollars). You can select any currency as holding currency then convert to other currencies in real time exchange rate.

## Highlight Features

- [x] Build in Calculator
- [x] Floating panel
- [x] Dark Mode
- [x] Reorder currencies
- [x] **Cocoapod** framework support

##ScreenShots

![Shot](./image/Shot.png)

##Library

#### Cocoapod

You can install the exchange rate library in following command in your ***Podfile***.

```ruby
use_frameworks!

target "Change Me!" do
	pod 'Currency_X_Lmcmz', '~> 0.0.3'
end
```

###### Example 

>  Get the exchange rate info list

```swift
ExchangeRate.getAllRateBaseAUD().done { list in
      ....
}
```

> Convert from one currency to an other currency (*Synchronization*)

```swift
ExchangeRate.shared.convertToAmountSync(from: .AUD, fromAmount: 100.0, to: .USD)
```

> Convert from one currency to an other currency (*Asynchronization*)

```swift
ExchangeRate.shared.convertToAmount(from: .AUD, fromAmount: 100.0, to: .USD).done { amount in
	...
}
```

